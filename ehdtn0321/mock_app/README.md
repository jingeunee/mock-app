Action
: 하나의 객체
: 모든 액션은 하나의 type를 지녀야 함( 액션의 이름과도 같은 존재)
: 리듀서가 액션을 전달받으면 이 값에 따라서 다른 작업을 하기도 함

Action Creator
 : 액션을 만들 때마다 객체를 그때 그때 만들기 힘들기 때문에
액션을 만드는 함수 

Action 선언 예시
 : export const INCREMENT = 'INCREMENT'
    -> 대문자로 선언해줌

Reducer
: 액션의 type에 따라 변화를 일으키는 함수
    ( 초기상태가 정의되어있어야 함)

Reducer Function
: state와 action을 파라미터로 가지는 함수
: switch문을 잉요해 action.type에 따라 상태에 변화를 일으킴
** state는 직접 변화를 일으키면 안되고,
   기존 state값에 덮어쓴 새 상태 객체를 만드는 방식 사용해야 함

Store
(redux의 가장 핵심적인 인스턴스)
: 이 안에 상태를 내장하고있고,
구독중인 함수들이 상태가 업데이트 될 때마다 다시 실행하게 해줌
:redux에서 createStore를 불러온 다음, 해당 함수의 파라미터로 만들어둔 reducer를 넣어줌
ex) import {createStore} from 'redux'

Provider
: react-redux라이브러리에 내장돼있는 컴포넌트
: react앱에 store를 쉽게 연동할 수 있도록 도와주는 컴포넌트

CounterConatiner 컴포넌트
: react-redux의 connect 함수를 이용, 컨테이너 컴포넌트를 store에 연결
  이 함수의 파라미터로 컴포넌트에 연결시킬 상태와, 액션 함수들을 전달, 컴포넌트를 리덕스 스토어에 연결시키는 또다른 함수 반환.
: 리턴된 함수 안에 프리젠테이셔널 컴포넌트를 파라미터로 전달해주면 리덕스 스토어에 연결된 컴포넌트가 새로 생성됨.
: 상태를 연결시킬땐 state,
  액션함수를 연결시킬땐 dispatch를
  파라미터로 전달받는 함수를 만들어서 
  객체를 반환하면 이를 props 로 사용 할 수 있게 됨.

