12/15
typescript
redux
helper library
를 이용한 'Todo list'

yarn create react-app my-app --template typescript로 typescript react 프로젝트 생성
( 생성 후 추가 할 경우
    yarn add typescript @types/node @types/react @types/react-dom @types/jest)
yarn eject
yarn add redux react-redux
yarn add redux-actions
yarn add immutable

### typescript는 tsx확장자를 사용
### store는 ts확장자 사용

# redux-action
 - 액션 생성 함수를 더 짧은 코드로 작성 가능.
 - swtich/case문이 아닌 handleAction함수를 사용하여 각 액션마다 업데이트 함수를 설정하는 형식으로 작성.

#### src/stroe/moduls/todo.tsx
  -> 필요한 액션에 대한 정의

import {createAction} from 'redux-actions';

const CHANGE_INPUT = 'todo/CHANGE_INPUT';
const INSERT = 'todo/INSERT';
const TOGGLE = 'todo/TOGGLE';
const REMOVE = 'todo/REMOVE';

export const changeInput = createAction(CHANGE_INPUT);
export const insert = createAction(INSERT);
export const toggle = createAction(TOGGLE);
export const remove = createAction(REMOVE);

##### index -> App -> container -> components

#### Immutable 규칙
객체는 Map
배열은 List
설정할땐 set
읽을땐 get
읽은다음에 설정 할 땐 update
내부에 있는걸 ~ 할땐 뒤에 In 을 붙인다: setIn, getIn, updateIn
일반 자바스크립트 객체로 변환 할 땐 toJS
List 엔 배열 내장함수와 비슷한 함수들이 있다 – push, slice, filter, sort, concat… 전부 불변함을 유지함
특정 key 를 지울때 (혹은 List 에서 원소를 지울 때) delete 사용

=============================================================
12/16
<REDUX MIDDLEWARE>

const loggerMiddleware = store => next => action {
  // ...내용
}

next : store.disptch과 비슷한 역할,
차이점
 - next(action)을 했을 때에는 바로 리듀서로 넘어가거나, 미들웨어가 더있 면 다음 미들웨어 처리가 되도록 진행
 - store.dispatch : 처음부터 다시 액션이 디스패치,  현재 미들웨어를 다시 번 처리하게 됨


현재 상태 기록 후 방금 전달 받은 액션을 기록, 다음 리듀서에 의해 액션이 처리된 다음의 스토어 값 기록 

[미들웨어 적용하기]
 미들웨어는 store를 생성할 때 설정.
 redux 모듈 안에 있는 applyMiddleware를 사용하여 설정 가능
 
ex)
  const store = createStore(rootReducer, applyMiddleware(logger));

  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <App />
        ...
  )

 'redux-logger'
 $ yarn add redux-logger

index.tsx에 적용후 실행

<redux-thunk>
비동기 작업을 처리할 때 이용하는 미들웨어.
액션 객체가 아닌 함수를 dispatch할 수 있음.

[example_ setTimeout을 이용해 딜레이 주기]

\\src/index.tsx
  import { applyMiddleware, createStore} from 'redux';
  import { composeWithDevTools } from 'redux-devtools-extension';
  import ReduxThunk from 'redux-thunk';
  import logger from 'redux-logger';

  import rootReducer from './store/modules';

  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(ReduxThunk,logger))
  );
index.tsx에 작성

\\src/store/modules/todos.ts
  export const actionCreatorsAsync = (input: string) => (dispatch:any) => {
    setTimeout(() => dispatch(actionCreators.create(input)),1000);
  }
추가

\\src/containers/TodoListContatiner.tsx
  import { TodoItemData , actionCreators as todosActions, actionCreatorsAsync } from '../store/modules/todos';
import 후
interface Props{}에
  TodosActionsAsync : typeof actionCreatorsAsync;
추가

  const {TodosActions} = this.props;
  --> const { TodosActions, TodosActionsAsync, input} = this.props;
변경
  
하단 connect()에 
  TodosActionsAsync : bindActionCreators(actionCreatorsAsync, dispatch)
추가

<callback/Promise>
*callback 함수
함수 타입의 값을 파라미터로 넘겨줘서, 파라미터로 받은 함수를 특정 작업이 끝난 후 해당 작업을 호출해주는 것.

ex)
  function work(callback) {
    setTimeout(() => {
      const start = Date.now();
      for (let i = 0; i < 1000000000; i++) {}
      const end = Date.now();
      console.log(end - start + 'ms');
      callback();
    }, 0);
  }
  console.log('작업 시작!');
  work(() => {
    console.log('작업이 끝났어요!')
  });
  console.log('다음 작업');
출력_
  > 작없 시작!
  > 다음 작업
  > 498ms
  > 작업이 끝났어요!

*Promise
비동기 작업을 좀 더 편하게 처리할 수 있도록 ES6에 도입된 기능.
콜백 함수로 비동기 작업을 처리하게 되면 작업이 많아질 경우 코드가 쉽게 난잡해짐. 비동기적으로 처리해야 하는 일 이 많아질 수록, 코드의 깊이가 계속 싶어지는 현상이 있지만, Promise를 사용하면 코드의 깊이가 깊어지는 현상을 방지.

ex)
  const myPromise = new Promise((resolve, reject) => {
    // 구현..
  })


