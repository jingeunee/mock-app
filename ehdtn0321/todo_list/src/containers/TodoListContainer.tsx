import { List } from 'immutable'
import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { StoreState } from '../store/modules';
import { TodoItemData , create, remove, toggle, changeInput, list} from '../store/modules/todos';

import TodoList from '../components/TodoList';

interface Props{
  todoItems: List<TodoItemData>;
  input: string;
  create: typeof create;
  remove: typeof remove;
  toggle: typeof toggle;
  changeInput: typeof changeInput;
  list : typeof list;
}

class TodoListContainer extends React.Component<Props> {
  componentDidMount(){
    this.props.list();
  }

  onCreate = () => {
    const { create, input} = this.props;
    create(input);
  }
  onRemove = (id: number) => {
    const {remove} = this.props;
    remove(id);
  }
  onToggle = (id: number, tf:boolean) => {
    const {toggle} = this.props;
    toggle(id,tf);
  }
  onChange = (e: React.FormEvent<HTMLInputElement>) => {
    const { value } = e.currentTarget;
    const { changeInput } = this.props;
    changeInput(value);
  }
  render() {
    const { input, todoItems } = this.props;
    const { onCreate, onRemove, onToggle, onChange } = this;

    return (
        <TodoList
            input={input}
            todoItems={todoItems}
            onCreate={onCreate}
            onRemove={onRemove}
            onToggle={onToggle}
            onChange={onChange}
        />
    );
  }
}

export default connect(
  ({todos}:StoreState) => ({
      input:todos.input,
      todoItems:todos.todoItems,
  }),
  (dispatch) =>  ({
      create: bindActionCreators(create, dispatch),
      remove: bindActionCreators(remove, dispatch),
      toggle: bindActionCreators(toggle, dispatch),
      changeInput: bindActionCreators(changeInput, dispatch),
      list: bindActionCreators(list, dispatch),
  })
)(TodoListContainer);