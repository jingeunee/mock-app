import React from 'react';
import './App.css';
import TodoListContainer from './containers/TodoListContainer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Redux Todo_List</h1>
        <h2>with. typescript</h2>
      </header>
      <body className="App-body">
        <TodoListContainer />
      </body>
    </div>
  );
}

export default App;
