import {combineReducers} from 'redux';
import todos, {TodosState} from './todos';

const rootReducer = combineReducers({ todos });

export default rootReducer;

export interface StoreState{
    todos: TodosState
}