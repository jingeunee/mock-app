import { Record, List, fromJS } from 'immutable';
import { createAction, handleActions, Action } from 'redux-actions';
import TodoItem from 'src/components/TodoItem';

// action type 정의
// const CREATE = 'todos/CREATE';
const CREATE_SUCCESS = 'todos/CREATE_SUCCESS';
const CREATE_FAILURE = 'todos/CREATE_FAILURE';
// const REMOVE = 'todos/REMOVE';
const REMOVE_SUCCESS = 'todos/REMOVE_SUCCESS';
const REMOVE_FAILURE = 'todos/REMOVE_FAILURE';
// const TOGGLE = 'todos/TOGGLE';
const TOGGLE_SUCCESS = 'todos/TOGGLE_SUCCESS';
const TOGGLE_FAILURE = 'todos/TOGGLE_FAILURE';
const CHANGE_INPUT = 'todos/CHANGE_INPUT';
// const CHANGE_SUCCESS = 'todos/CHANGE_SUCCESS';
// const CHANGE_FAILURE = 'todos/CHANGE_FAILURE';
const LIST_REQUEST = 'todos/LIST_REQUEST';
const LIST_SUCCESS = 'todos/LIST_SUCCESS';
const LIST_FAILURE = 'todos/LIST_FAILURE';
const EDIT = 'todos/EDIT';



// 각각의 payload type 지정
type EditPayload = number;
type ChangeInputPayload = string;
// type CreatePayload = string;
// type TogglePayload = number;

//// action-type 반환하는 함수 만들기
// export const actionCreators = {
//   create : createAction<CreatePayload>(CREATE),
//   remove : createAction<RemovePayload>(REMOVE),
//   toggle : createAction<TogglePayload>(TOGGLE),
//   changeInput : createAction<ChangeInputPayload>(CHANGE_INPUT)
// };


//list
export const list = () => async (dispatch:any) => {
  try{
    dispatch(createAction(LIST_REQUEST)());
    const req = await fetch('http://192.168.0.187:8080/todos',{method:'get'}).then(res=> res.json());
    console.log(req);
    dispatch(createAction(LIST_SUCCESS)(fromJS(req)));
  } catch (err) {
    dispatch(createAction(LIST_FAILURE)());
  }
}

//create
export const create = (input: string) => async (dispatch:any) => {
  try{
    // dispatch(actionCreators.create(input));
    // dispatch(createAction<CreatePayload>(CREATE)(input)); 
    const create = await fetch('http://192.168.0.187:8080/todos', {method:'POST', body:JSON.stringify({text:input}), headers:{"Content-type":"application/json"}}).then(res=> res.json());
    console.log(create);
    dispatch(createAction(CREATE_SUCCESS)(fromJS(create)));
  } catch(err) {
    dispatch(createAction(CREATE_FAILURE)());
  }
}

//remove
export const remove = (id: number) => async (dispatch:any) => {
  try{
    // dispatch(actionCreators.remove(input));
    // dispatch(createAction<RemovePayload>(REMOVE)(id));
    await fetch('http://192.168.0.187:8080/todos/'+id, {method: 'DELETE'});
    dispatch(createAction(REMOVE_SUCCESS)(id));
  } catch(err) {
    dispatch(createAction(REMOVE_FAILURE)());
  }
}

//toggle
export const toggle = (id: number, tf: boolean) => async (dispatch:any) => {
  // dispatch(actionCreators.toggle(input));
  // dispatch(createAction<TogglePayload>(TOGGLE)(input));
  try{
    const click = await fetch('http://192.168.0.187:8080/todos/'+id, {method: 'PUT', body:JSON.stringify({done:tf}), headers:{"Content-type":"application/json"}})
    .then(res => res.json());
    console.log(click)
    dispatch(createAction(TOGGLE_SUCCESS)(id));
  } catch{
    dispatch(createAction(TOGGLE_FAILURE)(id));
  }
}

export const changeInput = createAction<ChangeInputPayload>(CHANGE_INPUT);

//edit
export const edit = createAction<EditPayload>(EDIT);
  // dispatch(actionCreators.changeInput(input));

interface TodoItemDataParams{
  id: number;
  text: string;
  done: boolean;
  edit: boolean;
};

const TodoItemRecord = Record<TodoItemDataParams>({
    id : 0,
    text : '',
    done : false,
    edit : false
});

export class TodoItemData extends TodoItemRecord {};


interface ITodosState {
  todoItems: List<TodoItemData>;
  input: string;
}

const TodosStateRecord = Record<ITodosState>({
  todoItems : List(),
  input : ''
});

export class TodosState extends TodosStateRecord{}

const initialState = new TodosState();

// reducer 함수 작성
export default handleActions<TodosState, any>({
    //각 액션에 대한 업데이트 함수
  [CREATE_SUCCESS] :(state, action) => {
    // const a = state.get('todoItems');
    // const c =  a.push(action.payload);
    // return state.set('todoItems', c);
    return state
      .set('input', '')
      .update('todoItems', todoItems => todoItems.push(action.payload)); //reducer에서는 오는 파라미터값은 다 payload로 정해짐
  },
  //  [CREATE]: (state, action:Action<CreatePayload>): TodosState => {
  //   const text = action.payload; // input
  //   const newItem = new TodoItemData()
  //     .set('text', text)
  //     .set('id', state.get('todoItems').size + 1);

  //    return state
  //     .set('input', '')
  //     .update('todoItems', todoItems => todoItems.push(newItem));
  //  },
  [REMOVE_SUCCESS] : (state, action) => {
    const index = state.get('todoItems').findIndex(i => i.get('id') === action.payload);
    return state.deleteIn(['todoItems', index])
    //  [REMOVE]: (state, action:Action<RemovePayload>): TodosState => {
      //    return state.update(
        //      'todoItems',
        //      (todoItems: List<TodoItemData>) => todoItems.filter(
          //        t => t ? t.id !== action.payload : false
          //      )
          //    )
        },
        
   [TOGGLE_SUCCESS]: (state, action) => {
     const index = state.get('todoItems').findIndex(i => i.get('id') === action.payload);
     return state.updateIn(['todoItems', index, 'done'], done => !done)
   },
   [CHANGE_INPUT]: (state, action: Action<ChangeInputPayload>): TodosState => {
     return state.set('input', action.payload);
   },
   [LIST_SUCCESS]: (state, action): TodosState => {
     return state.set('todoItems', action.payload);
   },
  }, 
  initialState
);