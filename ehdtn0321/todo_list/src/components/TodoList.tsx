import * as React from 'react';
import { TodoItemData } from '../store/modules/todos';
import { List } from 'immutable';
import TodoItem from './TodoItem'

interface Props{
  input : string;
  todoItems: List<TodoItemData>;
  onCreate(input : string): void;
  onRemove(id: number): void;
  onToggle(id: number, tf: boolean): void;
  onChange(e: any): void;
}

const TodoList: React.SFC<Props> = ({
  input, todoItems, onCreate, onRemove, onToggle, onChange
}) => {
  const todoItemList = todoItems.map(
    todo => todo?(
      <TodoItem
        key={todo.get('id')}
        done={todo.get('done')}
        onToggle={() => onToggle(todo.get('id'),!todo.get('done'))}
        onRemove={() => onRemove(todo.get('id'))}
        onChange={() => onChange(todo.get('id'))}
        text={todo.get('text')}
      />
    ) : null
  );

  return (
    <div>
      <h1>ToodList</h1>
      <form 
        onSubmit={
          (e: React.FormEvent<HTMLFormElement>) => {
            e.preventDefault();
            onCreate(input);
          }
        }
      >
        <input onChange={onChange} value={input} />
        <button type="submit">추가하기</button>
      </form>
      <ul>
        {todoItemList}
      </ul>
    </div>
  )
}

export default TodoList;