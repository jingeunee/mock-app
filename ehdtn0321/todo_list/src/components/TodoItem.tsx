import * as React from 'react';

interface Props{
  text:string;
  done: boolean;
  onToggle(): void;
  onRemove(): void;
  onChange(): void;
}


const TodoItem: React.SFC<Props> = ({text, done, onToggle, onRemove, onChange }) => (
  <li>
    <button
      onClick={onToggle}
      style={{
        textDecoration:done ? 'line-through' : 'none'
      }}>
      {text}
    </button>
      <span style={{marginLeft:'0.5rem'}} onClick={onRemove}>[지우기]</span>
  </li>
);

export default TodoItem;