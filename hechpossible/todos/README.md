# Redux Counter 예제
1. redux가 순수함수인 이유  
    -순수 함수란 동일한 인자가 주어졌을때 항상 동일한 결과를 반환하는 것  
     즉, 함수 내의 변수 외에 외부의 값을 참조, 의존하거나 변경하지 않아야함.  
    -리덕스는 두 객체의 '메모리 위치'를 비교하여 이전 객체가 새 객체와 동일한지 여부를 단순 체크한다.  
     만약 리듀서 내부에서 이전 객체의 속성을 변경하면 새 상태와 이전 상태가 모두 동일한 객체를 가리킨다.  
     그렇게 되면 리덕스는 아무것도 변경되지 않았다고 판단하고 동작하지 않는다.  

2. redux를 사용하는 이유  
    -순수 리액트 프로젝트의 경우 'ROOT' > 'A' > 'B' > 'C' 형식으로 프로젝트가 제작된다.  
     이렇게 작성된 프로젝트의 문제점은 'C'의컴포넌트의 값을 전달하고 싶을때 'A'와'B'를 거쳐야 한다.  
     그렇게되면 유지 관리 비용이 증가하게 된다.  
    -반면 리덕스는 '리덕스 스토어'를 컴포넌트 바깥에 만들어 효율적으로 상태관리가 가능해진다.  

3. redux Project 만들기  
    1. yarn create react-app '프로젝트명'
    2. init commit 실행
    3. yarn eject
    4. yarn add react-redux redux

4. 프로젝트 작성 순서  
    1. 'App.js'에 react-redux에 들어있는 'Provider'를 사용, store import
    2. 컴포넌트 폴더 하위에 'Count.js(뷰)'작성
    3. store > modules 하위에 counter.js 모듈 작성 < 리듀서를 만들어서 내보는 곳, 액션 타입 정의 및 액션 생성함수 생성
    4. store > modules 하위 index.js에 'combineReducers'로 리듀서 합치기 < 현재는 counter뿐이지만 여러 리듀스를 작성뒤 하나의 리듀서로 합칠수있다.
    5. store > configure.js 작성 < 스토어를 만드는 함수 configure를 만들어서 export한다.
    6. store > index.js 작성 5번에 만든 configure함수를 사용하여 스토어를 만들고 내보내준다.
    7. containers > CounterContainers.js 작성 < 리덕스와 연동된 컴포넌트

