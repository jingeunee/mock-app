import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StoreState } from "../store/modules";
import { getList } from "../store/modules/todos";

const TodoList = () => {
    const dispatch = useDispatch();   
    const { list } =  useSelector((store : StoreState)=>{
        return {
            list : store.todos.get('todoItems')
        }
    });

    useEffect(() => {
        dispatch(getList());
    }, [])

    return (
        <div>
            list
            <ul>
                {list.map(item => {
                    return (
                        <li>
                            {item.get('text')} . {item.get('createdAt')}
                        </li>
                    )
                })
                }
            </ul>
        </div>
    );
}

export default TodoList;