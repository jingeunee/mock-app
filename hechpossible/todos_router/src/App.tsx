import { Component } from 'react';
import { Route , Switch } from 'react-router-dom';

import TodoDetail from './pages/TodoDetail';
import TodoList from './pages/TodoList';
import TodoResister from './pages/TodoResister';

class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact={true} path="/" component={TodoList} />
          <Route path="/detail" component={TodoDetail} />
          <Route path="/resister" component={TodoResister} />
        </Switch>
      </div>
    )
  }
}

export default App;
