import { Record, List, fromJS} from 'immutable';
import { createAction, handleActions, Action } from 'redux-actions';
import { ThunkAction } from 'redux-thunk';
import axios from 'axios';

const LIST_REQ = 'todos/LIST_REG';
const LIST_SUC = 'todos/LIST_SUC';
const LIST_FAI = 'todos/LIST_FAI';

type ListSucPayload = TodoItemDataParams[];

export function getList(): ThunkAction<void, TodosState, null, Action<ListSucPayload>>{
  return async dispatch => {
    try {
      dispatch(createAction(LIST_REQ)()); // req

      const data = await axios
        .get('http://192.168.0.187:8080/todos')
        .then(res => res.data);

      dispatch(createAction(LIST_SUC)(fromJS(data)));
    } catch (error) {
      dispatch(createAction(LIST_FAI)(error));
    }
  }
}

interface TodoItemDataParams {
  id: number;
  text: string;
  done: boolean;
  createdAt: string;
}
const TodoItemRecord = Record<TodoItemDataParams>({
  id: 0,
  text: '',
  done: false,
  createdAt: ''
})

export class TodoItemData extends TodoItemRecord {}

interface ITodosState {
  todoItems: List<TodoItemData>;
  input: string;
}
const TodosStateRecord = Record<ITodosState>({
  todoItems: List(),
  input: ''
})

export class TodosState extends TodosStateRecord {}

const initialState = new TodosState();
export default handleActions<TodosState, any>(
  {
    [LIST_REQ]: (state) => {
      return state;
    },
    [LIST_SUC]: (state, action) => {
      return state
        .set('todoItems', action.payload);
    },
    [LIST_FAI]: (state) => {
      return state
        .set('todoItems', List());
    },
  },
  initialState,
);