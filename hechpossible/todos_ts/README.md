# 리액트 리덕스 TypeScirpt TodoList 프로젝트

1. typescript란  
    - 자바스크립트의 기능 + 타입체크 기능  

2. TypeScript로 프로젝트 작성하기  
    1. yarn install -g typescript  
    2. yarn create react-app [프로젝트명] --template typescript  

3.  TodoList 프로젝트 작성  
    1. components > TodoItem.tsx 컴포넌트 작성  
    2. components > TodoList.tsx작성 > 뷰가 되는 부분  
    3. store > moduels > todos.ts 작성  
    4. store > modules > index.ts 작성 > 스토어의 상태타입 정의  
    5. containers > TodoListContainer.tsx 작성 > state, action connect부분

# 리액트 리덕스 미들웨어 적용

1. redux-logger 라이브러리 적용  
    - yarn add redux-logger
    - Could not find a declaration file for module 'react-redux'. 에러 발생시  
      npm install --save-dev @types/react-redux 실행  
2. redux-thunk 라이브러리 적용  
    - yarn add redux-thunk  

redux-thunk란  
- 리덕스에서 비동기 작업을 처리할 때 사용하는 미들웨어  
- 액션 객체가 아닌 함수를 디스패치 할수있다.

# RestAPI통신 적용

1. RestAPI란  
  - 월드 와이드웹과 같은 하이퍼미디어 시스템을위한 소프트웨어 아케텍쳐중 하나의 형식  
  - 클라이언트로 하여금 HTTP프로토콜을 사용해 서버의 정보에 접근 및 변경을 가능케한다.  

  - Rest기반 아키텍쳐에서 자주 사용하는 메소드  
    - GET ={url}<조회 list => return[]  
    - POST ={url}<추가, 추가하고자하는 body데이터가 필요함 ex)POST body={text:string} create => return{}  
    - PUT ={url/id}<상태변경,  변경하고자하는 body데이터가 필요함 exPUT body={done:boolean} update => return{}  
    - DELETE ={url/id} <삭제 return없음  

2. fetch란  
    - 서버에 네트워크 요청을 보내고 새로운 정보를 받아오는 함수  
    -  promise를 기반으로 되어있어 상태에 따른 로직을 추가하고 처리하는데 최적화 되어있음  
    
    기본 문법  
    
     fetch(url+id,{    <상태변경이나 삭제할때 키값을 주소 마지막에 붙힘  
        method: 'GET', OR POST OR PUT OR DELETE  
        headers:{'Content-Type': 'application/json'},  
        body: JSON.stringify({done}), <추가하거나 변경하고자 하는 데이터가 있을때 분힘  
      })
      .then(response => response.json())  
        
     JSON.stringfy() > 자바스크립트 값을 JSON문자열로 변환  
     JSON.parse() > JSON문자열을 자바스크립트 값을 변환  