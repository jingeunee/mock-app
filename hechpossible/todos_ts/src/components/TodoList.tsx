import * as React from 'react';
import TodoItem from './TodoItem';
import { TodoItemData } from '../store/modules/todos';
import { List } from 'immutable';
// import { useSelector, useDispatch } from 'react-redux';


interface Props {
  input: string;
  todoItems: List<TodoItemData>;
  onCreate(): void;
  onRemove(id: number): void;
  onToggle(id: number, done: boolean): void;
  onChange(e: any): void;
}
// const sleep = (n : number) => new Promise(resolve => setTimeout(resolve, n));

const TodoList: React.FunctionComponent<Props> =  ({
  input, todoItems, onCreate, onRemove, onToggle, onChange
}) => {
  const todoItemList =
    todoItems.map(
    todo => todo ? (
      <TodoItem
        key={todo.get('id')}
        done={todo.get('done')}
        onToggle={() => onToggle(todo.get('id'), !todo.get('done'))}
        onRemove={() => onRemove(todo.get('id'))}
        text={todo.get('text')}
      />
    ) : null
    
  );

  return (
    <div>
      <h1>Todo List</h1>
      <form 
        onSubmit={
          (e: React.FormEvent<HTMLFormElement>) => {
            e.preventDefault();
            onCreate();
          }
        }
      >
        <input onChange={onChange} value={input} />
        <button type="submit">일정등록</button>
      </form>
      <ul>
        {todoItemList}
      </ul>
    </div>
  );
};

export default TodoList;