import modules from './modules'
import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import ReduxThunk from 'redux-thunk';

export default function configureStore() {
    const store = createStore(
        modules,
        applyMiddleware(ReduxThunk, logger)
    );
    return store;
}