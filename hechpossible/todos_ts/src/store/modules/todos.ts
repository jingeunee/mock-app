import { Record, List, fromJS} from 'immutable';
import { createAction, handleActions, Action } from 'redux-actions'
import { ThunkAction } from 'redux-thunk';

const LIST_REQ = 'todos/LIST_REG';
const LIST_SUC = 'todos/LIST_SUC';
const LIST_FAI = 'todos/LIST_FAI';

const REMOVE_REQ = 'todos/REMOVE_REQ';
const REMOVE_SUC = 'todos/REMOVE_SUC';
const REMOVE_FAI = 'todos/REMOVE_FAI';

const CREATE_REQ = 'todos/CREATE_REQ';
const CREATE_SUC = 'todos/CREATE_SUC';
const CREATE_FAI = 'todos/CREATE_FAI';

const TOGGLE_REQ = 'todos/TOGGLE_REQ';
const TOGGLE_SUC = 'todos/TOGGLE_SUC';
const TOGGLE_FAI = 'todos/TOGGLE_FAI';

// const TOGGLE = 'todos/TOGGLE';
const CHANGE_INPUT = 'todos/CHANGE_INPUT';

type CreatePayload = string;
type RemovePayload = number;
type TogglePayload = number;
type ChangeInputPayload = string;
type ListSucPayload = TodoItemDataParams[];

export const actionCreators = {
  // create : createAction<CreatePayload>(CREATE),
  // remove : createAction<RemovePayload>(REMOVE),
  // toggle : createAction<TogglePayload>(TOGGLE),
  changeInput : createAction<ChangeInputPayload>(CHANGE_INPUT),

  // list_req : createAction(LIST_REQ),
  // list_suc : createAction<ListSucPayload>(LIST_SUC),
  // list_fai : createAction(LIST_FAI)

};


export function remove(id : number): ThunkAction<void, TodosState, null, Action<RemovePayload>> {
  return async dispatch => {
    // console.log('1초뒤 삭제');
    // setTimeout(() => dispatch(createAction<RemovePayload>(REMOVE)(id)) , 1000)
    try {
      dispatch(createAction(REMOVE_REQ)());

      await fetch('http://192.168.0.187:8080/todos/'+id,{
        method: 'DELETE',
        headers:{'Content-Type': 'application/json'}
      });

      // const findInx = mock.findIndex(obj => obj.id === id);
      // const findInx = Object.keys(id).indexOf(data);
      // if (findInx > -1) {
      //   mock.splice(findInx, 1);
      // } else {
      //   throw new Error('error');
      // }

      dispatch(createAction(REMOVE_SUC)(id));
    } catch (error) {
      dispatch(createAction(REMOVE_FAI)());
    }
  }
}

export function thunkToggle(id: number, done: boolean): ThunkAction<void, TodosState, null, Action<TogglePayload>>{
  return async dispatch =>{
    // setTimeout(() => dispatch(createAction<TogglePayload>(TOGGLE)(id)) , 2000)
    try{

      dispatch(createAction(TOGGLE_REQ)());
      const updated = await fetch('http://192.168.0.187:8080/todos/'+id,{
        method: 'PUT',
        headers:{'Content-Type': 'application/json'},
        body: JSON.stringify({done}),
      })
      .then(response => response.json())
      
      dispatch(createAction(TOGGLE_SUC)(fromJS(updated)));
    }catch(error) {
      dispatch(createAction(TOGGLE_FAI)());
    }
  }
}

export function create(text : string): ThunkAction<void, TodosState, null, Action<CreatePayload>>{
  return async dispatch =>{
    try{
      dispatch(createAction(CREATE_REQ)());

      const newdata = await fetch('http://192.168.0.187:8080/todos/',{
        method: 'POST',
        headers:{'Content-Type': 'application/json'},
        body: JSON.stringify({text}),
      })
      .then(response => response.json())
      
      dispatch(createAction(CREATE_SUC)(fromJS(newdata)));
    }catch(error) {
      dispatch(createAction(CREATE_FAI)());
    }

    // try{

    //   dispatch(createAction(CREATE_REQ)());

    //   const newdata = {
    //     id : mock.length + 1,
    //     text : text,
    //     done : false
    //   }

    //   console.log('ID:' + newdata.id , 'text:' + newdata.text, 'done:' + newdata.done);

    //   mock.push(newdata);
  
    //   dispatch(createAction(CREATE_SUC)(fromJS(mock)));

    // }catch(error) {
    //   dispatch(createAction(CREATE_FAI)());
    // }
  }
}

// const mock = [
//   {
//     id: 0,
//     text: '집에가기',
//     done: false,
//   },
//   {
//     id: 1,
//     text: '출근하기',
//     done: false,
//   },
//   {
//     id: 2,
//     text: 'test',
//     done: false,
//   },
// ];

export function getList(): ThunkAction<void, TodosState, null, Action<ListSucPayload>>{
  return async dispatch => {
    // dispatch(createAction(LIST_REQ)()); // req

    // fetch('http://192.168.0.187:8080/todos',{
    //   method: 'GET',
    //   headers:{'Content-Type': 'application/json'}
    // })
    // .then(response => dispatch(createAction(LIST_SUC)(fromJS(response.json()))))
    // .catch(error => dispatch(createAction(LIST_FAI)(error)));

    try {
      dispatch(createAction(LIST_REQ)()); // req
  
      const data = await fetch('http://192.168.0.187:8080/todos',{
        method: 'GET',
        headers:{'Content-Type': 'application/json'}
      })
      .then(response => response.json());

      dispatch(createAction(LIST_SUC)(fromJS(data)));
    } catch (error) {
      dispatch(createAction(LIST_FAI)(error));
    }
  }
}


interface TodoItemDataParams {
  id: number;
  text: string;
  done: boolean;
}
const TodoItemRecord = Record<TodoItemDataParams>({
  id: 0,
  text: '',
  done: false
})

export class TodoItemData extends TodoItemRecord {}

interface ITodosState {
  todoItems: List<TodoItemData>;
  input: string;
}
const TodosStateRecord = Record<ITodosState>({
  todoItems: List(),
  input: ''
})

export class TodosState extends TodosStateRecord {}

const initialState = new TodosState();

export default handleActions<TodosState, any>(
  {
    // [CREATE]: (state, action: Action<CreatePayload>) => {
    //   const text = action.payload; // input: string
    //   const params = new TodoItemData()
    //     .set('text', text)
    //     .set('id', state.get('todoItems').size + 1);

    //   return state
    //     .set('input', '') // input 초기화
    //     .update('todoItems', todoItems => todoItems.push(params)); // created!
    // },

    // [REMOVE]: (state, action: Action<RemovePayload>) => {
    //   return state
    //     .update('todoItems', todoItems => todoItems.filter(t => t ? t.id !== action.payload : false))
    // },

    // [TOGGLE]: (state, action: Action<TogglePayload>) => {
    //   const index = state.todoItems.findIndex(t => t ? t.id === action.payload : false);
    //   return state
    //     .updateIn(['todoItems', index, 'done'] , done => !done)

    // },

    [CREATE_REQ]: (state, action) => {
      return state;
    },

    [CREATE_SUC]: (state, action) => {
      console.log(action.payload)

      return state
        .set('input', '')
        .update('todoItems', todoItems => todoItems.push(action.payload))
    },
    
    [CREATE_FAI]: (state, action) => {
      return state.set('todoItems', List());
    },


    [TOGGLE_REQ]: (state, action) => {
      return state;
    },

    [TOGGLE_SUC]: (state, action) => {
      const index = state.todoItems.findIndex(t => t.get('id') === action.payload.get('id'));
      
      return state
        .setIn(['todoItems', index] , action.payload)
      // return state
      //   .updateIn(['todoItems', index, 'done'] , done => !done)
    },
    [TOGGLE_FAI]: (state, action) => {
      return state.set('todoItems', List());
    },

    [CHANGE_INPUT]: (state, action: Action<ChangeInputPayload>) => {
      return state
        .set('input', action.payload);
    },
    

    [LIST_REQ]: (state, action) => {
      return state;
    },
    [LIST_SUC]: (state, action) => {
      return state
        .set('todoItems', action.payload);
    },
    [LIST_FAI]: (state) => {
      return state.set('todoItems', List());
    },

    [REMOVE_REQ]: (state, action) => {
      return state;
    },
    [REMOVE_SUC]: (state, action) => {
      const index = state.todoItems.findIndex(t => t.get('id') === action.payload);
      return state.update('todoItems', todoItems => todoItems.delete(index))
    },

    [REMOVE_FAI]: (state, action) => {
      return state
        .set('todoItems', List());
    },
  },
  initialState,
);