import * as React from 'react';
import TodoList from '../components/TodoList';
import { connect } from 'react-redux';
import { StoreState } from '../store/modules';
import {
    TodoItemData,
    actionCreators as todosActions,
    remove,
    thunkToggle,
    getList,
    create,
} from '../store/modules/todos';
import { bindActionCreators } from 'redux';
import { List } from 'immutable';

interface Props {
    todoItems : List<TodoItemData>;
    input : string;
    TodosActions : typeof todosActions;
    remove : typeof remove;
    ThunkToggle : typeof thunkToggle;
    ThunkgetList : typeof getList
    Create : typeof create
}

class TodoListContainer extends React.Component<Props> {

    // state = {
    //     fetchlist : []
    // }

    // UNSAFE_componentWillMount(){
    //     fetch('http://192.168.0.187:8080/todos',{
    //         method: 'post',
    //         headers:{

    //         }
    //     })
    //     .then((response) => response.json()) 
    //     .then((response) => console.log(response))
    //     .then((response) => this.setState({fetchlist : response}))
    // }

    componentDidMount() {
        this.props.ThunkgetList();
    }


    onCreate = () => {
        const { input } = this.props;
        this.props.Create(input);
    }

    onRemove = (id : number) => {
        this.props.remove(id);
    }

    onToggle = (id : number, done: boolean) => {
        // const { TodosActions } = this.props;
        // TodosActions.toggle(id); 
        const { ThunkToggle } = this.props;
        ThunkToggle(id, done);

    }
    onChange = (e: React.FocusEvent<HTMLInputElement>) => {
        const { value } = e.currentTarget;
        const { TodosActions } = this.props;    
        TodosActions.changeInput(value);
    }
    render(){
        const { input, todoItems } = this.props;
        const { onCreate, onRemove, onToggle, onChange } = this;

        return (
            <TodoList
                input={input}
                todoItems={todoItems}
                onCreate={onCreate}
                onRemove={onRemove}
                onToggle={onToggle}
                onChange={onChange}
            />
        );
    }
}

export default connect(
    ({ todos } : StoreState) => ({
        input : todos.input,
        todoItems : todos.todoItems,
    }),
    (dispatch) => ({
        TodosActions : bindActionCreators(todosActions, dispatch),
        remove : bindActionCreators(remove, dispatch),
        ThunkToggle : bindActionCreators(thunkToggle, dispatch),
        ThunkgetList : bindActionCreators(getList, dispatch),
        Create : bindActionCreators(create, dispatch)
    })
)(TodoListContainer);
