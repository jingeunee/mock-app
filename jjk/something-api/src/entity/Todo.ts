import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Todo {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: "text"})
  text: string;

  @Column({type: "boolean", default: false})
  done: boolean;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
