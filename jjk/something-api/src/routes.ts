import { Context } from "koa";

import { todoGetAllAction } from "./controller/TodoGetAllAction";
import { todoCreateAction } from "./controller/TodoCreateAction";
import { todoStatusChangeAction } from "./controller/TodoStatusChangeAction";
import { todoDestroyAction } from "./controller/TodoDestroyAction";

interface IAppRoutes {
  path: string;
  method: 'get'|'post'|'put'|'delete';
  action: (ctx: Context) => void;
}

export const AppRoutes: IAppRoutes[] = [
  { // list
    path: "/todos",
    method: "get",
    action: todoGetAllAction,
  },
  { // create
    path: "/todos",
    method: "post",
    action: todoCreateAction,
  },
  { // change status (done)
    path: "/todos/:id",
    method: "put",
    action: todoStatusChangeAction,
  },
  { // destroy
    path: "/todos/:id",
    method: "delete",
    action: todoDestroyAction,
  },
];
