import 'reflect-metadata';
import cors from '@koa/cors';
import Router from '@koa/router';
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import koaHelmet from 'koa-helmet';
import KoaLogger from 'koa-logger';

import { AppRoutes } from './routes';
import { createConnection } from 'typeorm';

const PORT = process.env.PORT || '8080';

createConnection().then(async () => {
  const app = new Koa();
  const router = new Router();
  
  app.proxy = true;
  
  app.use(bodyParser());
  app.use(koaHelmet());
  app.use(cors());
  app.use(KoaLogger());
  
  AppRoutes.forEach(route => router[route.method](route.path, route.action));
  
  app.use(router.routes());
  app.use(router.allowedMethods());
  
  app.listen(PORT, () => console.log(`[INF] start application ${PORT}`));
}).catch(error => console.log("TypeORM connection error: ", error));
