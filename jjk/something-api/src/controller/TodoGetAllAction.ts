import { Context } from "koa";
import { getManager } from "typeorm";

import { Todo } from "../entity/Todo";

export async function todoGetAllAction(ctx: Context) {
  const todoRepo = getManager().getRepository(Todo);
  const todos = await todoRepo.find();

  ctx.status = 200;
  ctx.body = todos;
}
