import { Context } from "koa";
import { getManager } from "typeorm";

import { Todo } from "../entity/Todo";

export async function todoStatusChangeAction(ctx: Context) {
  const { id } = ctx.params;
  const body = ctx.request.body as {done: boolean};
  
  const todoRepo = getManager().getRepository(Todo);
  await todoRepo.update(id, body);
  
  const updated = await todoRepo.findOne(id);

  ctx.status = 200;
  ctx.body = updated;
}
