import { Context } from "koa";
import { getManager } from "typeorm";

import { Todo } from "../entity/Todo";

export async function todoCreateAction(ctx: Context) {
  const body = ctx.request.body as {text: string};

  const todoRepo = getManager().getRepository(Todo);
  const newTodo = todoRepo.create(body);
  await todoRepo.save(newTodo);

  ctx.status = 200;
  ctx.body = newTodo;
}
