import { Context } from "koa";
import { getManager } from "typeorm";

import { Todo } from "../entity/Todo";

export async function todoDestroyAction(ctx: Context) {
  const { id } = ctx.params as {id: string};

  const todoRepo = getManager().getRepository(Todo);
  const {affected} = await todoRepo.delete(id);
  if (!affected) {
    ctx.status = 404;
    return;
  }
  
  ctx.status = 204;
}
